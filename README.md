<h1>HC2I_MGA_APP</h1> 

<b>Managing Gard Application( API / back office )</b>
<ul>
<li>Create user system including user roles, permissions, groups and authentication<b>(18h)</b></li>
<li>Create a shifting system that include 
    <ul>
     <li>Manage user shifts set (from , to , break time) by day , ability to duplicate a shift for a specific date range<b>(35h)</b></li>
     <li>Time counter<b>(25h)</b></li> 
     <li>Start/stop shift (location validation)<b>(10h)</b></li>
     <li>Start/stop break (location validation)<b>(5h)</b></li>
     <li>Approve or reject Shifts</li>
     <li>Calculate total hours by day/week/month and user/group<b>(22h)</b></li>
     <li>Shift details by date/user/zone.<b>(32h)</b></li>
     <li>Report and statistics by date/user/zone.<b>(45h)</b></li>
     <li>Manage absences and handler user replacement (User can request absence with sending notification to admins to approve/reject or replace)<b>(34h)</b></li>
    </ul>
</li>
<li>Manage User (crud) check shift history/absence/locations/zones<b>(50h)</b></li>
<li>Manage zones and work areas (crud/location), user can be linked to many zones<b>(45h)</b></li>
<li>Create API endpoint and function to react with the mobile app <b>(50h)</b></li>
<li>Console and receive notifications<b>(30h)</b></li>
</ul>

<h3>The time estimation is between 60 and 75 days<h3/>

<small style="float: right"> <a href="https://hc2i.tech/"> &#169;Hc2i Maroc 	&#174;</a></small>